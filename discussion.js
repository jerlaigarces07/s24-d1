// Query Operators

	// $gt (greater than) / $gte (greater than or equal to)
	/* Syntax:
		db.collectionName.find({ field: { $gt: value }});
		db.collectionName.find({ field: { $gte: value }});
	*/
		db.users.find({ age: { $gt: 80 }});
		db.users.find({ age: { $gte: 70 }});

	// $lt (less than) / $ lte (less than or equal to)
	/* Syntax:
		db.collectionName.find({ field: { $lt: value }});
		db.collectionName.find({ field: { $lte: value }});
	*/
		db.users.find({ age: { $lt: 50 }});
		db.users.find({ age: { $lte: 50 }});

	// $ne (not equal)
	/* Syntax:
		db.collectionName.find({ field: { $ne: value }});
	*/
		db.users.find({ age: { $ne: 82 }});

	// $in
	/* Syntax:
		db.collectionName.find({ field: { $in: [value, ...] }});
	*/

		db.users.find({ lastName: { $in: ["Hawking", "Doe", "Gates"] }});

// (Logical) Query Operator

 	// $or
 	/* Syntax:
 		db.collectionName.find({ $or: [{ fieldA: value },{ fieldB: valueB }] });
 	*/
 		db.users.find({ $or: [{ firstName: "Neil" },{ age: 25 }] });
 		db.users.find({ 
 			$or: 
 				[{ firstName: "Neil" },
 				{ age: { $gt: 30 } }]
 			});

 	// $and
 	/* Syntax:
		db.collectionName.find({ $and: [{ fieldA: value },{ fieldB: valueB }] });
 	*/
 		db.users.find({ 
 			$and: 
 				[{ age: { $ne: 82 } }, 
 				{ age: { $ne: 76 } }]
 			});

// Field Projections
	// Inclusions - fields are included to the resulting documents.
		/* Syntax:
			db.collectionName.find({ field: value }, { field: 1 });
		*/
			db.users.find({ firstName: { $ne: "Jane"} }, { firstName: 1, lastName: 1 });

	// Exclusions - fields are excluded to the resulting documents.
 		/* Syntax:
			db.collectionName.find({ field: value }, { field: 0 });
		*/
			db.users.find({ firstName: { $ne: "John"} }, { _id: 0, contact: 0, department: 0 });

	// Inclusion/exclusion of nested document fields
		/* Syntax:
			db.collectionName.find(
				{ field: value }, 
			{
				field.nestedField: 1, [field.nestedField: 0, ...] 
			});

			db.collectionName.find(
				{ field: value }, 
			{ 
				field.nestedField: 1, [field.nestedField: 0, ...] 
			});
		*/
		 
			db.users.find(
				{ age: {
					$gt:75
				}},
				{
					"firstName": 1,
					"lastName": 1,
					"contact.phone": 1
				}
			)

// (Evaluation) Query Operator
	// $regex --> pattern
	/* Syntax:
		db.collectionName.find({ field: $regex: 'pattern', $options: 'optionValue'});
	*/
		db.users.find({ "firstName": { $regex: "N"} }); // case-sensitive
		db.users.find({ "firstName": { $regex: "N", $options: '$i'} }); // case-insensitive